import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { PhotoboothRoutingModule } from './photobooth-routing.module';
import { PhotoboothComponent } from './photobooth.component';
import { CapturePhotoComponent } from './capture-photo/capture-photo.component';


@NgModule({
  declarations: [PhotoboothComponent, CapturePhotoComponent],
  imports: [
    CommonModule,
    PhotoboothRoutingModule
  ]
})
export class PhotoboothModule { }
