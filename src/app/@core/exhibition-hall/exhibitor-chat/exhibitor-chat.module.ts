import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ExhibitorChatComponent } from './exhibitor-chat.component';
import { RouterModule } from '@angular/router';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';



@NgModule({
  declarations: [ExhibitorChatComponent],
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    RouterModule
  ],
  exports:[ExhibitorChatComponent]
})
export class ExhibitorChatModule { }
