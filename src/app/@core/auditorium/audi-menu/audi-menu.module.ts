import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AudiMenuComponent } from './audi-menu.component';
import { RouterModule } from '@angular/router';


@NgModule({
  declarations: [AudiMenuComponent],
  imports: [
    CommonModule,
    RouterModule,
  ],
  exports: [AudiMenuComponent]
})
export class AudiMenuModule { }
